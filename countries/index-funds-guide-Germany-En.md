
# Table of Contents

1.  [Index funds mini-guide (Tax residents in Germany version)](#orge9f9f5c)
    1.  [General information](#orgb0b76a8)
    2.  [Stocks](#orge6e7807)
    3.  [Bonds](#org4465c28)



<a id="orge9f9f5c"></a>

# Index funds mini-guide (Tax residents in Germany version)


<a id="orgb0b76a8"></a>

## General information

The pdf version of this guide (so you can click on the non-explicit hyperlinks) can be found at <https://gitlab.com/joywolove/index-funds-guide/-/tree/master/countries> with the name `index-funds-guide-Germany-En.pdf`.

Please refer to the [index funds guide](https://gitlab.com/joywolove/index-funds-guide/) for general information (currently only in Spanish, but will be updated on 2020Q4-2021Q1). 

As a summary, we can invest in two types of financial securities: **stocks** (long term. For money we won't require sooner than 5 years from now) and **bonds** (shorter term. For money we might need at some point in the next 5 years). 

Define your **Emergency Fund/Reserve** (generally between 3 and 9 months of expenses, depending on several factors), determine your **asset allocation** between stocks and bonds (e.g. 80% stocks, 20% bonds), and you're ready to invest. 


<a id="orge6e7807"></a>

## Stocks

For **stocks**, we will follow the [MSCI ACWI index](https://www.msci.com/acwi) (Developed Markets + Emerging Markets), which is a capital weighted index for the entire world <sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>. We can buy a single ETF that tracks that index, if we find it, or we can replicate it through two ETFs in their correct proportion (recently, around ~90% Developed and ~10% Emerging). 

-   **Developed:** [LU1737652237](https://www.justetf.com/de-en/etf-profile.html?index=MSCI%252BWorld&groupField=none&sortField=ter&sortOrder=asc&spc=71&replicationType=replicationType-full&from=search&isin=LU1737652237) (lowest TER <sup><a id="fnr.2" class="footref" href="#fn.2">2</a></sup> among [these](https://www.justetf.com/de-en/find-etf.html?tab=overview&index=MSCI%252BWorld&groupField=none&sortField=ter&sortOrder=asc&spc=71&replicationType=replicationType-full)).
-   **Emerging:** [LU1737652583](https://www.justetf.com/de-en/etf-sparplan-vergleich/msci-emerging-markets-etf.html?isin=LU1737652583) (lowest TER among these).

The best provider (broker) for these specific ETFs <sup><a id="fnr.3" class="footref" href="#fn.3">3</a></sup> is DKB, given by [this](https://www.justetf.com/de-en/etf-sparplan-vergleich/msci-world-etf.html?isin=LU1737652583) [analysis](https://www.justetf.com/de-en/etf-sparplan-vergleich/msci-world-etf.html?isin=LU1737652237). 


<a id="org4465c28"></a>

## Bonds

For **bonds**, we will follow a similar strategy, choosing a bond fund. In this case, given the lower volatility of bonds, diversification is not our top priority, so we choose a bond fund of European countries. 

-   European bonds: [LU1931975152](https://www.justetf.com/de-en/etf-sparplan-vergleich/msci-emerging-markets-etf.html?isin=LU1931975152) (lowest TER among [these](https://www.justetf.com/de-en/etf-profile.html?index=MSCI%252BWorld&groupField=none&sortField=ter&sortOrder=asc&spc=71&replicationType=replicationType-full&from=search&isin=LU1931975152)).


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> It excludes only the smallest economies (Frontier Markets), so it approximates very well the **actual** world index (ACWI + Frontier Markets index). Mutual Funds and ETFs for said index are harder to find, so we stick with the ACWI.

<sup><a id="fn.2" href="#fnr.2">2</a></sup> Total Expense Ratio. It tells us, in %, the **cost** of said Fund/ETF. Therefore, we want to minimise it, without resorting to *synthetic* replication Funds/ETFs.

<sup><a id="fn.3" href="#fnr.3">3</a></sup> Exchange Traded Fund)
