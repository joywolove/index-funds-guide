* ToDo [0/11]

  [ ] Add gitignore file. 

  [ ] Make it so diacritics (e.g. á) are recognised, to make contributions easier. 

  [ ] Add missing info (date, etc). See ./tex/z-vault.tex for the template info. 

  [ ] Create the English-language version of the Spain guide. 

  [ ] Create the English-language version of the Germany guide. 

  [ ] Reach out to the online investing and FIRE communities for help. 

  [ ] Split chapter c02 into c02 and c03. {001}

  [ ] Margins are way too big. Reduce them. 

  [ ] Graphs to showcase ROI of stocks. 

  [ ] F.A.Q. document. 

    [ ] Compile and store all the questions you are aware of. 

    [ ] Draft the answers. 

  [ ] Bond funds, as part of the F.A.Q., although mentioned on the main guide. Include, graphical, comparisons between stocks and bonds. (e.g. with [[https://markets.ft.com/data/funds/uk/compare]])


* Sections and their filenames

  Note; We use "input" instead of "include" to avoid flushing the next section
  to a new page, as the guide must be as short as possible and not intimidating. 

  ./tex/c0a-quote.tex      Initial quote from Warren Buffet.
  ./tex/c01-why-what.tex   Why to invest and on what (index funds) (COUNTRY-independent).
  ./tex/c02-Funds.tex      Which Funds/ETFs exactly? (COUNTRY-dependent).
  ./tex/c02-Funds.tex      Through which broker? (COUNTRY-dependent). {001}
  ./tex/c99-end.tex        Conclusion and recap (should be COUNTRY-independent).


* Changelog 

  1.2   2020-05-30   Added to source control. Modularity changes. 

  1.1   2018-09-07   Changed Developed fund after BNP imposed new fees on "clean" funds (e.g. Vanguard). Added table of contents, version history, informational links for the relevant funds, IFA movie. Moved some links to footnotes. Minor changes. Corrected typos. 

  1.0   2017-11-28   Creation
